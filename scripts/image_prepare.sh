#!/usr/bin/env bash


openstack overcloud container image prepare \
    --namespace=registry.access.redhat.com/rhosp13 \
    --push-destination=192.168.24.1:8787 \
    --prefix=openstack- \
    -e /usr/share/openstack-tripleo-heat-templates/environments/services/neutron-ovn-ha.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/octavia.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/sahara.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/barbican.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/barbican-backend-simple-crypto.yaml \
    -e /home/stack/templates/46-barbican-config.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible.yaml \
    --set ceph_namespace=registry.access.redhat.com/rhceph \
    --set ceph_image=rhceph-3-rhel7 \
    --tag-from-label {version}-{release} \
    --output-env-file=/home/stack/templates/overcloud_images.yaml \
    --output-images-file /home/stack/local_registry_images.yaml

sudo openstack overcloud container image upload \
    --config-file /home/stack/local_registry_images.yaml \
    --verbose
