#!/bin/bash

set -eux

#
## Start deployment $(date +%F)
#

# If we have any enviornment variables that we need to set for deploying
# the undercloud, we can set that here

# Run the installation command
openstack undercloud install 

#
## End deployment $(date +%F)
#
